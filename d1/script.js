//get post data
fetch('https://jsonplaceholder.typicode.com/posts')	
.then(response => response.json())
.then(data => showPosts(data))
//.then(data => console.log(data))
.catch(err => console.log(err))


const showPosts = (posts) =>{
	let postEntries = '';
	posts.forEach(element =>{
		postEntries += `
		<div id="post-${element.id}">
		<h3 id="post-title-${element.id}">${element.title}</h3>
		<p id="post-body-${element.id}">${element.body}</p>
		<button onclick="editPost(${element.id})">Edit</button>
		<button onclick="deletePost(${element.id})">Delete</button>
		</div>`
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
	//return postEntries
}

const editPost = (postId) =>{
	let title = document.querySelector(`#post-title-${postId}`).innerHTML;
	let body = document.querySelector(`#post-body-${postId}`).innerHTML;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#txt-edit-id').value = postId;
	document.querySelector('#btn-submit-update').removeAttribute('disabled')

	//update
	let updateForm = document.querySelector('#form-edit-post')
	updateForm.addEventListener('submit',(e)=>{
		e.preventDefault();
		//console.log(id)
		fetch('https://jsonplaceholder.typicode.com/post/1',{
			method:'PUT',
			body: JSON.stringify({
				id:postId,
				title:document.querySelector('#txt-edit-title').value,
				body:document.querySelector('#txt-edit-body').value,
				userId:1
			}),
			headers:{'Content-type': 'application/json; charset=UTF-8'	}
		})
		.then((response) => response.json())
		.then((data) => {
			console.log(data);
			alert('Post Updated')
		})
	})
}

const deletePost = (postId) =>{
	alert('Delete this post ' +  postId)
}


// addPost
let addPostForm = document.querySelector('#form-add-post')
addPostForm.addEventListener('submit',(e) =>{
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts',{
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers:{'Content-type': 'application/json; charset=UTF-8'}
	})
	.then(response => response.json())
	.then(data => {
	console.log(data)
	document.querySelector('#txt-title').value = "";
	document.querySelector('#txt-body').value = "";
	alert('Post Added')
	})
})



// const p = new Promise((resolve, reject) =>{
// 	//async operation
// 	//resolve(1)
// 	reject(new Error('Message'))
// })

// p
// 	.then(result => console.log('Result: ', result))
// 	.catch(err => console.log('Error: ', err.message))